const Transactions = require("../model/Transactions");
const Account = require("../model/Account");
const User = require("../model/User");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const Client = require("../model/Client");
const Admin=require("../model/Adminn");
const Saler = require("../model/Saler");
const { findOneAndDelete } = require("../model/Account");

// Admin me
module.exports.adminGetMiddleware = auth;

module.exports.adminGetFunction = async (req, res) => {
  try {
    let admin = await Admin.findOne({ user: req.user.id }).populate("user");
    res.status(200).json(admin);
  } catch (err) {
    res.status(500).json({ msg: "error fetching acount" });
  }
};


// partie Client
// tous les clients
module.exports.clienGetMiddleware = auth;

module.exports.clientGetFunction = async (req, res) => {
  try {
    let clients = await Client.find().populate(
        "user"
      );
    res.status(200).json(clients);
  } catch (err) {
    res.status(500).json({ msg: "error fetching client" });
  }
};

// les detail du client
module.exports.clientOneGetMiddleware = auth;

module.exports.clientOneGetFunction = async (req, res) => {
    let data={}
  try {
    let client = await Client.findOne({ user: req.params.id }).populate(
        "user"
      );
    let user=await User.findOne({_id:client.user})
    let account=await Account.findOne({user:user})

    let transactions=await Transactions.find({user:user})

    
    transactions = transactions.map((transaction) => transaction.toJSON());
    console.log(transactions);
    let transactionAll= transactions.length;
    data.client=client;
    data.account=account;
    data.transaction=transactionAll

   
    res.status(200).json(data);
  } catch (err) {
    res.status(500).json({ msg: "error fetching client" });
  }
};

// nombre de client
module.exports.clientAllGetMiddleware = auth;

module.exports.clientAllGetFunction = async (req, res) => {
  try {
    
    let clients = await Account.find();
    if(!clients){
        let clientAll=0
        res.status(200).json({data:clientAll,msg:"nombre de compte"});

    }

    clients = clients.map((client) => client.toJSON());
    console.log(clients);
    let clientAll=clients.length;

    res.status(200).json({data:clientAll,msg:"nombre de clients"});
  } catch (err) {
    res.status(500).json({ msg: "error fetching client" });
  }
};
// fin partie client 


// Partie account
// nombre de compte
module.exports.accountAllGetMiddleware = auth;

module.exports.accountAllGetFunction = async (req, res) => {
  try {
    
    let accounts = await Account.find();

    accounts = accounts.map((acount) => acount.toJSON());
    console.log(accounts);
    let accountAll= accounts.length;

    res.status(200).json({data:accountAll,msg:"nombre de compte"});
  } catch (err) {
    res.status(500).json({ msg: "error fetching compte" });
  }
};
// nombre de transaction 

module.exports.transactionAllGetMiddleware = auth;

module.exports.transactionAllGetFunction = async (req, res) => {
  try {
    
    let transactions=await Transactions.find()

    if(!transactions){
        let transactionAll=0
        res.status(200).json({data:transactionAll,msg:"nombre de compte"});

    }

    
    transactions = transactions.map((transaction) => transaction.toJSON());
    console.log(transactions);
    let transactionAll= transactions.length;

    res.status(200).json({data:transactionAll,msg:"nombre de compte"});
  } catch (err) {
    res.status(500).json({ msg: "error fetching compte" });
  }
};

// Partie Saler 
// all saler
module.exports.salerGetMiddleware = auth;

module.exports.salerGetFunction = async (req, res) => {
  try {
    let salers = await Saler.find().populate(
        "user"
      );
    res.status(200).json(salers);
  } catch (err) {
    res.status(500).json({ msg: "error fetching client" });
  }
};
// one specific saler
module.exports.salerOneGetMiddleware = auth;

module.exports.salerOneGetFunction = async (req, res) => {
  try {
    let saler = await Saler.findOne({user:req.params.id}).populate(
        "user"
      );
    res.status(200).json(saler);
  } catch (err) {
    res.status(500).json({ msg: "error fetching client" });
  }
};

// manager
// get all manager 
 
module.exports.headSalesManagerProfileMeGetMiddleware = auth;

module.exports.headSalesManagerProfileMeGetFunction = async (req, res) => {
  try {
    let users = await User.find({
      role: ["ClientManager", "SalerManager"],
    });

    res.status(200).json(users);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in fetching user");
  }
};

// create Manager saler et client


module.exports.signupPostMiddleware = [
    check("phone", "please add  a valide number").not().isEmpty(),
    check("email", "please add a valide email").isEmail(),
    check("role", "please add a valide role").custom(
      (value, { req }) =>
        User.schema.path("role").enumValues.indexOf(value) >= 0 ? true : false
    ),
  ];
  module.exports.signupPostFunction = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }
  
    const { phone, email, role } = req.body;
    try {
      // verification de l'existance de l'utilisateur
      let user = await User.findOne({
        email,
      });
      if (user) {
        return res.status(411).json({
          msg: "un utilisateur avec ce mail existe deja",
        });
      }
      user = await User.findOne({
        phone,
      });
      if (user) {
        return res.status(412).json({
          msg: "un utilisateur avec ce numero existe deja dans le systeme",
        });
      }
     
  
       
      //let password = randomString(8); //TODO: a decommenter
      let password = "7777"; //TODO: a commenter
      user = new User({
        phone,
        email,
        password,
        role,
      });
  
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);
  
      await user.save();
  
      sendRegistrationEmail(email, { email: email, password: password });
  
      const data = {
        user: {
          id: user.id,
        },
      };
  
      jwt.sign(
        data,
        "randomString",
        // {
        //   expiresIn: 10000,
        // },
        (err, token) => {
          if (err) throw err;
          res.status(200).json({
            token,
          });
        }
      );
    } catch (err) {
      console.log(err);
      res.status(500).send("probleme lors de l'inscription");
    }
  };

module.exports.deleteMiddleware=auth;
module.exports.deleteClient=(req,res)=>{
   try{
    let query={user:req.params.id}
    await .findOneAndDelete({query})
    res.status(200).send("manager deleted");

   }catch{
    res.status(500).send("probleme de suppression")
   }
}