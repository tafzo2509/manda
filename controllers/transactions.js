const Transactions = require("../model/Transactions");
const Account = require("../model/Account");
const User = require("../model/User");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const { findOne } = require("../model/user");

// tous les transactions du client
module.exports.transactionGetMiddleware = auth;

module.exports.transactionsGetFunction = async (req, res) => {
  try {
    let transactions = await Transactions.find({ user: req.user.id });
    res.status(200).json(transactions);
  } catch (err) {
    res.status(500).json({ msg: "vous n'avez pas peut etre fait pas de transaction" });
  }
};

// une transaction specifique par id 
module.exports.transactionGetMiddleware = auth;

module.exports.transactionsGetFunction = async (req, res) => {
  try {
    let transactions = await Transactions.findOne({ _id: req.params.id });
    res.status(200).json(transactions);
  } catch (err) {
    res.status(500).json({ msg: "cette transaction n'existe pas" });
  }
};


// enrigistrement d'une transaction

module.exports.transactionPostMiddleware = auth;

module.exports.transactionsPostFunction = async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }
  
    let transaction = { ...req.body };
    let dest=await User.findOne({phone:req.body.phone});
  
    try {
      transactions = new Transactions({
        user: req.user.id,
        destinator:dest,
        ...transaction,
      });
  
      await transactions.save();
  
      res.status(200).json({
        msg: "transaction enrigistrer",
        data: transactions,
      });
    } catch (err) {
      console.log(err);
      res.status(500).send("erreur d'enrigistrement de la transaction");
    }
};