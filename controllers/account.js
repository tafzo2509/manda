const Transactions = require("../model/Transactions");
const Account = require("../model/Account");
const User = require("../model/User");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

// tous 
module.exports.accountGetMiddleware = auth;

module.exports.accountGetFunction = async (req, res) => {
  try {
    let account = await Account.findOne({ user: req.user.id });
    res.status(200).json(account);
  } catch (err) {
    res.status(500).json({ msg: "error fetching acount" });
  }
};

// 
module.exports.accountDestinatorPatchMiddleware = [auth];
module.exports.accountDestinatorPatchFunction = async (req, res) => {
    try{
        
        let user=await User.findOne({phone:req.body.phone});
        let query={user:user._id};
        let account=await Account.findOne(query);
        let sum = await account.sum+req.body.sum;

        await Account.updateOne(query, sum, (err) => {
            if (err) {
              console.log(err);
              res.status(500).json({ error: "Error on saving" });
            }
          });
          res.status(200).json({
            msg: "account update",
            data: {
              account: await Account.findOne(query),
            },
          });



    }catch(err){
        console.log(err);
        res.status(500).json({ msg: "Error in Fetching User" });
    }
  
};

module.exports.accountMePatchMiddleware = [auth];
module.exports.accountMePatchFunction = async (req, res) => {
    try{
        let query={user:req.user.id};
        let account=await Account.findOne(query);
        let sum = await account.sum-req.body.sum;

        await Account.updateOne(query, sum, (err) => {
            if (err) {
              console.log(err);
              res.status(500).json({ error: "Error on saving" });
            }
          });
          res.status(200).json({
            msg: "account update",
            data: {
              account: await Account.findOne(query),
            },
          });



    }catch(err){
        console.log(err);
        res.status(500).json({ msg: "Error in Fetching User" });
    }
  
};