const Transactions = require("../model/Transactions");
const Account = require("../model/Account");
const User = require("../model/User");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const Client=require("../models/Client")

// client 
module.exports.clientGetMiddleware = auth;

module.exports.clientGetFunction = async (req, res) => {
  try {
    let client = await Client.findOne({ user: req.user.id });
    res.status(200).json(client);
  } catch (err) {
    res.status(500).json({ msg: "vous n'avez pas peut etre fait pas de transaction" });
  }
};

// cree ou modifier un profil client
module.exports.clientPatchMiddleware = auth;
module.exports.clientPatchFunction = async (req, res) => {
    let client = { ...req.body };
  
    let query = { _id: req.user.id };
  
    await User.updateOne(query, client, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });
  
    let clientQuery = { user: req.user.id };
  
    let clt = Client.updateOne(clientQuery, client);
    if (!clt) {
      clientProfile = new Client({
        user: req.user.id,
        ...client,
      });
  
      await clientProfile.save();
    }
    res.status(200).json({
      msg: "Updated",
      data: {
        profile: await Client.findOne(clientQuery),
        user: await User.findOne(query),
      },
    });
  };
