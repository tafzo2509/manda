const express = require("express");
const router = express.Router();

const controller = require("../controllers/admin");


// information d;un utilisateur par id
router.get("/client", controller.clientOneGetMiddleware, controller.clientOneGetFunction);



module.exports = router;
