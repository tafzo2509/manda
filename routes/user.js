const express = require("express");
const router = express.Router();

const controller = require("../controllers/user");

// envoi code par telephone
router.post(
  "/code",
  controller.codePostMiddleware,
  controller.codePostFunction
);

// inscription
router.post(
  "/signup",
  controller.signupPostMiddleware,
  controller.signupPostFunction
);

// login
router.post(
  "/login",
  controller.loginPostMiddleware,
  controller.loginPostFunction
);

// information de l'utisateur connecte(via token)
router.get("/me", controller.meGetMiddleware, controller.meGetFunction);

// modification de l'utilisateur connecter
router.patch("/me", controller.mePatchMiddleware, controller.mePatchFunction);

// information d;un utilisateur par id
router.get("/:id", controller.userGetMiddleware, controller.userGetFunction);

// modification d'un utilisateur par id
router.patch(
  "/:id",
  controller.userPatchMiddleware,
  controller.userPatchFunction
);

module.exports = router;
