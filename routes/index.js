const user =require("./user");
const client =require("./client");
const admin =require("./admin");
const saler =require("./saler");
const transactions =require("./transactions");
const account =require("./account");
const accountManager =require("./accountManager");


module.exports=[{
    name:"user",
    router:user
},
// {
//     name:"client",
//     router:client
// },
{
    name:"admin",
    router:admin
},
// {
//     name:"saler",
//     router:saler
// },
// {
//     name:"transactions",
//     router:transactions
// },
// {
//     name:"account",
//     router:account
// },
// {
//     name:"accountManager",
//     router:accountManager
// }

]