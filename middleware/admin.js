const User = require("../model/User");

module.exports = function (req, res, next) {
  try {
    const role = User.findOne({ _id: req.user.id, role: "admin" });

    if (role) {
      next();
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Only admin do this operation" });
  }
};