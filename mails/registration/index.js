const transporter = require('../../config/mail');
const fs = require('fs');
const path = require('path');


module.exports = (to, data) => {
    let emailHtml = fs.readFileSync(path.join(__dirname + `/index.html`), 'utf-8')

    emailHtml = emailHtml
        .replace('#email', data.email)
        .replace('#password', data.password)

    const emailText = `
    votre email: ${data.email} 
    votre mot de pass: ${data.password} `;

    const emailData = {
        from: '"Mandaa Busness corp" ' + process.env.SMTP_LOGIN,
        to: to,
        subject: 'inscription avec success!',
        text: emailText,
        html: emailHtml
    }

    transporter.sendMail(emailData)
}