const mongoose = require("mongoose");
const { transformUser } = require("../scripts/transform");
const UserSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: [
      "admin",
      "client",
      "clientManager",
      "SalerManager",
      "super admin",
      "Saler"
    
    ],
  },
  surName: {
    type: String,
  },
  name: {
    type: String,
  },
  region:{
    type:String
  },

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  img: {
    type: String,
  },
});

UserSchema.options.toJSON ={
  transform: transformUser,
};

module.exports = mongoose.model("user", UserSchema);
