const mongoose = require("mongoose");

// account
const accountSchema = mongoose.Schema({
 
  // user
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  
   // somme
   sum: {
    type: Number,
    required: true,
  },
 
 

});

module.exports = mongoose.model("account", accountSchema);