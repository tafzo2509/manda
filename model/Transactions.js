
const mongoose = require("mongoose");
// transaction
const TransactionSchema = mongoose.Schema({
  // user
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
   // destinataire
  destinator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",    
  },
  // somme
  sum: {
    type: Number,
    required: true,
  },
  // data
  date: {
    type: Date,
    default: Date.now(),
  },

});
module.exports = mongoose.model("transaction", TransactionSchema);