const mongoose = require("mongoose");

// saler
const salerSchema = mongoose.Schema({
 
  // user
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  // carte d'identite
  CNI: {
    type: String,
    required: true,
  },
   // justificatif
   justify: {
    type: String,
    required: true,
  },

});

module.exports = mongoose.model("saler", salerSchema);