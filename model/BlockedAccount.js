const mongoose = require("mongoose");

// account
const blockedAccountSchema = mongoose.Schema({
 
  // user
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

   // somme
   sum: {
    type: Number,
    required: true,
  },

  date:{
    type:Date,
    required:true
  },
  status:{
    type:Boolean,
    required:true

  }
 
 

});

module.exports = mongoose.model("blockaccount", blockedAccountSchema);