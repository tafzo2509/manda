const mongoose = require("mongoose");

// SMS код для подтверждения номера
const CodeSchema = mongoose.Schema({
  // Четырехзначный код
  code: {
    type: String,
    required: true,
  },
  // Номер телефона
  phone: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("code", CodeSchema);
