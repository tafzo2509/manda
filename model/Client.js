const mongoose = require("mongoose");

// client
const clientSchema = mongoose.Schema({
 
  // user
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  // carte d'identite
  CNI: {
    type: String,
    required: true,
  },
 

});

module.exports = mongoose.model("client", clientSchema);